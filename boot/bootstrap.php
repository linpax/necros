<?php declare(strict_types=1);

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__ . '/../usr/share/autoload.php';

$loader->addClassMap([
//  'Micro\\Kernel\\System' => __DIR__ . '/../usr/share/linpax/framework/kernel/System.php',
//  'Micro\\Kernel\\ApplicationInterface' => __DIR__ . '/../usr/share/linpax/framework/kernel/ApplicationInterface.php',
]);

return $loader;
