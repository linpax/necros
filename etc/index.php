<?php declare(strict_types=1);

return [
    'container' => require __DIR__ . '/container.php'
];
