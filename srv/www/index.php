<?php declare(strict_types=1);


require __DIR__ . '/../../boot/bootstrap.php';


use Micro\Base\Kernel;
use App\Application;
use Zend\Diactoros\ServerRequestFactory;


\Http\Response\send( // sent response
    (new Application( // app
        new Kernel('debug', true) // kernel
    ))->run( // runner
        ServerRequestFactory::fromGlobals() // request
    )
);
