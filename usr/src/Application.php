<?php declare(strict_types=1);

namespace App;

use DI\ContainerBuilder;
use Micro\Base\Kernel;
use Micro\Base\Application as iApplication;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;


class Application implements iApplication
{
    /** @var Kernel */
    protected $kernel;
    /** @var ContainerInterface */
    protected $container;
    /** @var array */
    protected $config;


    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getKernel() : Kernel
    {
        return $this->kernel;
    }

    public function getConfig() : array
    {
        if (!$this->config) {
            $this->config = require __DIR__ . '/../../etc/index.php';
        }

        return $this->config;
    }

    /**
     * @return ContainerInterface
     * @throws \Exception
     */
    public function getContainer() : ContainerInterface
    {
        if (!$this->container) {
            try {
                $config = $this->getConfig();
                $definitions = !empty($config['container']) ? $config['container'] : [];

                $this->container = $this->makeContainer(false, false, $definitions);
            } catch (\Exception $e) {
                $this->container = null;
                throw $e;
            }
        }

        return $this->container;
    }


    public function run(RequestInterface $request) : ResponseInterface
    {
        $message = null;

        try {
            // @TODO: Replace to add router + resolver + move to web
            $helloWorld = $this->getContainer()->get('controller');
            $message = $helloWorld->rend()."\n".$this->getKernel()->getElapsedTime()."\n";

        } catch (\Exception $e) {
            $message = '<h1>Error - Unresolved exception error</h1><p>'.print_r($e, true).'</p>';
        } catch (\Throwable $t) {
            $message = '<h1>Error - Unresolved throwable error</h1><p>'.print_r($t, true).'</p>';
        } finally {
            $response = new Response();

            $response->getBody()->write( $message );

            return $response;
        }
    }

    /**
     * @param bool  $autoWiring
     * @param bool  $useAnnotations
     * @param array $definitions
     *
     * @return ContainerInterface
     * @throws \Exception
     */
    protected function makeContainer(bool $autoWiring = false, bool $useAnnotations = false, array $definitions = []) : ContainerInterface
    {
        $builder = new ContainerBuilder();

        $builder->useAutowiring($autoWiring);
        $builder->useAnnotations($useAnnotations);
        $builder->addDefinitions($definitions);

        return $builder->build();
    }
}
